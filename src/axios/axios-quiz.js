import axios from 'axios';

export default axios.create({
    baseURL: 'https://react-quiz-e1f77.firebaseio.com/'
});
